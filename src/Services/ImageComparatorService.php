<?php

namespace Drupal\image_comparator\Services;

class ImageComparatorService implements ImageComparatorServiceInterface {

  protected ImageComparator $imageComparator;

  public function __construct() {
    $this->imageComparator = new ImageComparator();
  }

  public function compare(string $image1, string $image2): float {
    return $this->imageComparator->compare($image1, $image2);
  }

}
