<?php

interface ImageComparatorServiceInterface {
  public function compare(string $image1, string $image2): float;

}
